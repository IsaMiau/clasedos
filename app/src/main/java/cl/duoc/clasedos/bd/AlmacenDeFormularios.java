package cl.duoc.clasedos.bd;

import java.util.ArrayList;

import cl.duoc.clasedos.entidades.Formulario;

/**
 * Created by DUOC on 18-03-2017.
 */

public class AlmacenDeFormularios {

    private static ArrayList<Formulario> formularios = new ArrayList<>();

    public static void agregarFormulario(Formulario formulario){
        formularios.add(formulario);
    }

    public static ArrayList<Formulario> getFormularios(){
        return formularios;
    }

    /* Para agregar elementos por defecto
    static{
      formularios.add(new Formulario());
    }
    */
}
