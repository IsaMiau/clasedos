package cl.duoc.clasedos.entidades;

/**
 * Created by DUOC on 18-03-2017.
 */

public class Formulario {

    private String nombre;
    private String rut;
    private int edad;
    private String telefono;


    public String getNombre() {
        return nombre;
    }

    public String getRut() {
        return rut;
    }

    public int getEdad() {
        return edad;
    }

    public String getTelefono() {
        return telefono;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
