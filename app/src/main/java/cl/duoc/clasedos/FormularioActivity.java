package cl.duoc.clasedos;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import cl.duoc.clasedos.bd.AlmacenDeFormularios;
import cl.duoc.clasedos.entidades.Formulario;

public class FormularioActivity extends AppCompatActivity {

    private EditText etNombre, etRut, etEdad, etTelefono;
    private Button btnGuardar, btnLimpiar;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        etNombre = (EditText) findViewById(R.id.etNombre);
        etRut = (EditText) findViewById(R.id.etRut);
        etEdad = (EditText) findViewById(R.id.etEdad);
        etTelefono = (EditText) findViewById(R.id.etTelefono);

        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }

        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarFormulario();
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void guardarFormulario() {
        String mensajeError = "";

        if (etNombre.getText().toString().length() < 1) {
            mensajeError += "Ingrese nombre \n";
        }
        if (etTelefono.getText().toString().length() < 12) {
            mensajeError += "Ingrese formato correcto de teléfono (+569XXXXXXX) \n";
        }
        if (!isValidarRut(etRut.getText().toString())) {
            mensajeError += "Ingrese un rut válido \n";
        }
        if(etEdad.getText().toString().length()>0){
            try{
                int edad = Integer.parseInt(etEdad.getText().toString());
                if(edad <= 0 && edad >= 120){
                    mensajeError += "Ingrese una edad válida de 1 a 120\n";
                }
            }
            catch(NumberFormatException e){
                mensajeError += "Ingrese una edad válida de 1  a 120 \n";
            }
        }
        else{
            mensajeError += "Ingrese una edad válida de 1  a 120 \n";
        }

        if(mensajeError.length() > 0){
            Toast.makeText(this, mensajeError, Toast.LENGTH_LONG).show();
        }
        else{

        }

       /* if ((etNombre.getText().toString().length() > 0)
                && (edad > 0 && edad <= 120)
                && (etRut.getText().toString().length() == 12 && etRut.getText().toString().length() > 0)
                && (etTelefono.getText().toString().length() > 0)) {
            Formulario nuevoForm = new Formulario();
            nuevoForm.setEdad(Integer.parseInt(etEdad.getText().toString()));
            nuevoForm.setNombre(etNombre.getText().toString());
            nuevoForm.setRut(etRut.getText().toString());
            nuevoForm.setTelefono(etTelefono.getText().toString());

            AlmacenDeFormularios.agregarFormulario(nuevoForm);
            limpiarCampos();
            Toast.makeText(this, "Formulario guardado correctamente", Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, "Ingrese Todos los Campos", Toast.LENGTH_SHORT).show();
        }*/
    }

    private void limpiarCampos() {
        etNombre.setText("");
        etRut.setText("");
        etEdad.setText("");
        etTelefono.setText("");
        etNombre.requestFocus();
    }

     private boolean validarRut(String rut) {

        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }



    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Formulario Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
